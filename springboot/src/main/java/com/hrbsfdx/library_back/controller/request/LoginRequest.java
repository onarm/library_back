package com.hrbsfdx.library_back.controller.request;

import lombok.Data;

@Data
public class LoginRequest {
    private Integer id;
    private String username;
    private String phone;
    private String email;
}

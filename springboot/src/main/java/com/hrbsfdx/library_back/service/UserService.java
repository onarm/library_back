package com.hrbsfdx.library_back.service;

import com.github.pagehelper.PageInfo;
import com.hrbsfdx.library_back.controller.request.BaseRequest;
import com.hrbsfdx.library_back.controller.request.UserPageRequest;
import com.hrbsfdx.library_back.pojo.User;

import java.util.List;


public interface UserService {

    List<User> list();

    PageInfo<User> page(BaseRequest baseRequest);

    void save(User user);

    User getById(Integer id);

    void update(User user);

    void deleteById(Integer id);
}

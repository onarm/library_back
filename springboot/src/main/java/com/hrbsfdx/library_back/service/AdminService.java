package com.hrbsfdx.library_back.service;

import com.github.pagehelper.PageInfo;
import com.hrbsfdx.library_back.controller.dto.LoginDTO;
import com.hrbsfdx.library_back.controller.request.BaseRequest;
import com.hrbsfdx.library_back.controller.request.LoginRequest;
import com.hrbsfdx.library_back.pojo.Admin;

import java.util.List;


public interface AdminService {

    List<Admin> list();

    PageInfo<Admin> page(BaseRequest baseRequest);

    void save(Admin obj);

    Admin getById(Integer id);

    void update(Admin obj);

    void deleteById(Integer id);

    LoginDTO login(LoginRequest request);
}

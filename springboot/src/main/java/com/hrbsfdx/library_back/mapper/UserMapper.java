package com.hrbsfdx.library_back.mapper;

import com.hrbsfdx.library_back.controller.request.BaseRequest;
import com.hrbsfdx.library_back.pojo.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {

//    @Select("select * from user")
    List<User> list();

    List<User> listByCondition(BaseRequest baseRequest);

    void save(User user);

    User getById(Integer id);

    void updateById(User user);

    void deleteById(Integer id);
}
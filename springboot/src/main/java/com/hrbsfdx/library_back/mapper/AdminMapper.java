package com.hrbsfdx.library_back.mapper;

import com.hrbsfdx.library_back.controller.request.BaseRequest;
import com.hrbsfdx.library_back.controller.request.LoginRequest;
import com.hrbsfdx.library_back.pojo.Admin;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AdminMapper {


    List<Admin> list();

    List<Admin> listByCondition(BaseRequest baseRequest);

    void save(Admin obj);

    Admin getById(Integer id);

    void updateById(Admin obj);

    void deleteById(Integer id);

    Admin getByUsernameAndPassword(LoginRequest request);
}
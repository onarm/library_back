import { createRouter, createWebHashHistory } from 'vue-router'
import Layout from '../views/Layout.vue'

const routes = [
    // ======= 登录 =======
    {
        path: '/login',
        name: 'login',
        component: () =>
            import ('../views/login/Login.vue')
    },
    // ======= 主页 =======
    {
        path: '/',
        name: 'Layout',
        redirect: '/home',
        component: Layout,
        children: [{
                path: 'home',
                name: 'Home',
                component: () =>
                    import ('../views/home/HomeView.vue')
            },
            // ======== User  ========
            {
                path: 'userList',
                name: 'UserList',
                component: () =>
                    import ('../views/user/User.vue')
            },
            {
                path: 'addUser',
                name: 'AddUser',
                component: () =>
                    import ('../views/user/AddUser.vue')
            },
            {
                path: 'editUser',
                name: 'EditUser',
                component: () =>
                    import ('../views/user/EditUser.vue')
            },
            // ======== Admin  ========
            {
                path: 'adminList',
                name: 'AdminList',
                component: () =>
                    import ('../views/admin/List.vue')
            },
            {
                path: 'addAdmin',
                name: 'AddAdmin',
                component: () =>
                    import ('../views/admin/Add.vue')
            },
            {
                path: 'editAdmin',
                name: 'EditAdmin',
                component: () =>
                    import ('../views/admin/Edit.vue')
            }
        ]
    },

]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

export default router
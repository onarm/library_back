import { createApp } from 'vue'
import App from './App.vue'
import router from './router'


import ElementUI from 'element-plus'
import 'element-plus/dist/index.css'
import ElementPlusIconsVue from '@element-plus/icons-vue'

import '@/assets/global.css'
createApp(App).use(router).use(ElementUI).mount('#app');